import os
import pandas as pd
import multiprocessing
import time
from os import listdir
from os.path import isfile, join
from PIL import Image, ImageChops
import imageio
import cv2
import numpy as np
from scipy import ndimage
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.ticker as ticker


from seuillage import img_threshold, change_contrast, change_contrast_folder, to_gray_scale, img_frombytes
from bbox_from_obj import bbx_from_mask, label_file_from_coord
from add_layers import add_layers, get_average_image, get_average_image_list,get_average_image_list_resize

def chunkIt(seq, num):
    avg = len(seq) / float(num)
    out = []
    last = 0.0

    while last < len(seq):
        out.append(seq[int(last):int(last + avg)])
        last += avg

    return out

def get_date_taken(path):
    return Image.open(path)._getexif()[36867]

def multi_proc_manager(image_list,target_function,ratio=1,area_threshold_min=0,area_threshold_max=1000):
    
    # print("Number of cpu : ", multiprocessing.cpu_count()) 
    cpu_num = int(multiprocessing.cpu_count() - 6)

    chunked_list = chunkIt(image_list, cpu_num)

    manager = multiprocessing.Manager()
    return_dict = manager.dict()

    procs = []

    proc_num=0

    for chunk in chunked_list:
        
        proc_num +=1

        # creating processes 
        proc = multiprocessing.Process(target=target_function, args=(chunk,return_dict,ratio,area_threshold_min,area_threshold_max)) 
        # proc = multiprocessing.Process(target=compare_image_to_average, args=(chunk, average_image,proc_num_abs, return_dict)) 
        procs.append(proc)
        proc.start()
  
    # starting processes
    for proc in procs:
        
        proc.join() 

    return return_dict

    # result_df = pd.concat(list(return_dict.values()))

    # print(result_df)
    # result_df.to_csv(outname, index=False)

    # all processes finished 
    # print("refine done!") 


def image_processing(image_path, return_dict, ratio = 0.1):

    image = Image.open(image_path)

    ############# Resize ################

    w,h=image.size
    newsize = (int(w*ratio),int(h*ratio))
    image = image.resize(newsize)

    ############ Threhold ###############

    threshold_array = np.array([ 
          [30, 30, 30],  # dark grey
          [0, 0, 0]])  # black

    mask = np.all(
    np.logical_and(
        np.min(threshold_array, axis=0) < image,
        image < np.max(threshold_array, axis=0)
    ),
    axis=-1
    )
    im_mask = img_frombytes(mask)
    # outname = image_path[:-4] + '-mask.jpg'
    # im_mask.save(outname)

    ########## Detect ################

    measurements = bbx_from_mask(mask,area_threshold_min = 1,area_threshold_max=1000)
    
    
    ########## returning length to tict for multiproc #########
    # print(len(measurements))
    # return len(measurements)
    return_dict[image_path] = len(measurements)
    print(return_dict)

    
def image_processing_multi(image_list, return_dict,ratio=1,area_threshold_min=1,area_threshold_max=1000):

    i = 0
    for image_path in image_list:
        
        i += 1
        print(str(i)+ "/" + str(len(image_list)))
        # image_processing(image_path,return_dict)
        image = Image.open(image_path)

        ############# Resize ################

        w,h=image.size
        newsize = (int(w*ratio),int(h*ratio))
        image = image.resize(newsize)

        ############ Threhold ###############

        # threshold_array = np.array([ 
        #     [30, 30, 30],  # dark grey
        #     [0, 0, 0]])  # black
        print(np.mean(np.array(image)))

        threshold_array = np.array([ 
            [255, 255, 255],  # white
            [200, 200, 200]])  # grey

        mask = np.all(
        np.logical_and(
            np.min(threshold_array, axis=0) < image,
            image < np.max(threshold_array, axis=0)
        ),
        axis=-1
        )

        im_mask = img_frombytes(mask)
        outname = image_path[:-4] + '-mask.jpg'
        im_mask.save(outname)

        ########## Detect ################

        measurements = bbx_from_mask(mask,area_threshold_min = area_threshold_min,
                                            area_threshold_max = area_threshold_max)
        
        
        ########## returning length to tict for multiproc #########
        # print(len(measurements))
        # return len(measurements)
        return_dict[image_path] = len(measurements)

        if i%10 == 0:
            time.sleep(10)
    # print(return_dict)

def image_processing_tiff(image_list, return_dict,ratio=1,area_threshold_min=100,area_threshold_max=1000):

    i = 0
    for image_path in image_list:
        
        i += 1
        print(str(i)+ "/" + str(len(image_list)))
        
        image = cv2.imread(image_path) #requires OpenCV to convert 16bits image


        ############# Resize ################

        # w,h=image.size
        # newsize = (int(w*ratio),int(h*ratio))
        # image = image.resize(newsize)

        # ###### for tif image ##########

        # tif_img = Image.open(image_path)
        # tif_img = tif_img.resize(newsize)
        # image = Image.new("RGB", tif_img.size)
        # image.paste(tif_img)

        ############ Threhold ###############

        # threshold_array = np.array([ 
        #     [30, 30, 30],  # dark grey
        #     [0, 0, 0]])  # black
        
        # print(np.mean(np.array(image)))
        # print(np.array(image).shape)

        threshold_array = np.array([ 
            [255, 255, 255],  # white
            [150, 150, 150]])  # grey

        mask = np.all(
        np.logical_and(
            np.min(threshold_array, axis=0) < image,
            image < np.max(threshold_array, axis=0)
        ),
        axis=-1
        )

        im_mask = img_frombytes(mask)
        outname = image_path[:-4] + '-mask.jpg'
        im_mask.save(outname)

        ########## Detect ################

        measurements = bbx_from_mask(mask,area_threshold_min = area_threshold_min,
                                            area_threshold_max = area_threshold_max)
        
        label_file_from_coord(measurements,image_path)
        
        
        ########## returning length to tict for multiproc #########
        # print(len(measurements))
        # return len(measurements)
        return_dict[image_path] = len(measurements)

        if i%10 == 0:
            time.sleep(10)
    # print(return_dict)



# origine = os.path.dirname(os.path.abspath(__file__))


if __name__ == "__main__":

    t0 = time.time()
    
    
    # dir_to_detect = 'test/'
    dir_to_detect = 'test_kimograph/'

    # image_list = sorted([dir_to_detect + f for f in os.listdir(dir_to_detect) if f.endswith('.JPG')])
    image_list = sorted([dir_to_detect + f for f in os.listdir(dir_to_detect) if f.endswith('.tif')])
    # print(image_list)

    # return_dict = multi_proc_manager(image_list,image_processing_multi)
    return_dict = multi_proc_manager(image_list,image_processing_tiff)

    # to plot dict
    lists = sorted(return_dict.items()) # sorted by key, return a list of tuples
    x, y = zip(*lists) # unpack a list of pairs into two tuples

    t1 = time.time()
    print((t1-t0)/60)

    print(max(set(y), key=y.count))
    print(sum(y)-2*len(y))



    fig, ax1 = plt.subplots()

    color = 'tab:red'
    ax1.set_xlabel('date')
    ax1.set_ylabel('number of objects', color=color)
    ax1.plot(x, y, color=color)
    ax1.tick_params(axis='y', labelcolor=color)
    ax1.tick_params(axis='x', rotation=90, labelsize=5)

    # plt.show()
    plt.savefig('object.png',dpi=150)
   




