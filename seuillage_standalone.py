import os
from os import listdir
from os.path import isfile, join
from PIL import Image
import numpy as np
from scipy import ndimage
import matplotlib.pyplot as plt

# c_yel = color_array = np.array([ 
#   [255, 255, 0],  # bright yellow
#   [150, 150, 150]])  # mid yellow

blur_radius = 1.0
threshold = 0
n_image = 0
# blur = False
blur = True


def img_frombytes(data):
    size = data.shape[::-1]
    databytes = np.packbits(data, axis=1)
    return Image.frombytes(mode='1', size=size, data=databytes)

def img_threshold(image,threshold_array):

    im = Image.open(image)
    width, height = im.size
    # width2 = int(width/4)
    # height2 = int(height/4)
    width2 = width
    height2 = height
    im2 = im.resize((width2,height2), Image.ANTIALIAS)
    im2 = np.asarray(im2)
    print(im2.shape)

    mask = np.all(
    np.logical_and(
        np.min(threshold_array, axis=0) < im2,
        im2 < np.max(threshold_array, axis=0)
    ),
    axis=-1
    )
    im_mask = img_frombytes(mask)
    im_mask.save(str(image[:-4])+'_thr_resized.jpg')
    
    if blur:
        imgf = ndimage.gaussian_filter(im_mask, blur_radius)
        labeled, nr_objects = ndimage.label(imgf > threshold) 
        print("Number of objects is {}".format(nr_objects))
    else:
        imgf = ndimage.gaussian_filter(im_mask, blur_radius)
        labeled, nr_objects = ndimage.label(mask > threshold) 
        print("Number of objects is {}".format(nr_objects))
    
    return mask

def change_contrast(img, level):
    factor = (259 * (level + 255)) / (255 * (259 - level))
    def contrast(c):
        return 128 + factor * (c - 128)
    return img.point(contrast)

# origine = os.path.dirname(os.path.abspath(__file__))
# origine = os.path.dirname('151_PANA')
files_to_list = sorted([f for f in os.listdir("151_PANA") if (f.endswith('.JPG')and not f.endswith('-contrast.JPG'))])
print(files_to_list)

for image_file in files_to_list:
    
    image_path = "151_PANA/" + image_file

    print(image_path)

    im_orig = Image.open(image_path)

    im_contrast = change_contrast(im_orig,-50)
    # outname = image_file[:-4] + "-contrast.JPG"
    outname = "contrast/" + image_file

    im_contrast.save(outname)