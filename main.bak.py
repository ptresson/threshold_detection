import os
import pandas as pd
import multiprocessing
import time
from os import listdir
from os.path import isfile, join
from PIL import Image, ImageChops
import numpy as np
from scipy import ndimage
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.ticker as ticker


from seuillage import img_threshold, change_contrast, change_contrast_folder, to_gray_scale, img_frombytes
from bbox_from_obj import bbx_from_mask, label_file_from_coord
from add_layers import add_layers, get_average_image, get_average_image_list,get_average_image_list_resize

def get_date_taken(path):
    return Image.open(path)._getexif()[36867]


def compare_image_to_average(chunk, average_image,proc_num, return_dict):

    im2 = average_image

    for image_path in chunk :
        
        im1 = Image.open(image_path)
        date = get_date_taken(image_path)
        # date_list.append(date) 

        diff = ImageChops.difference(im2, im1)
        # outname = 'out/' + image_path[:-4] + '-diff.jpg'
        # diff.save(outname)

        # binary = diff.convert('1')
        # outname = image_path[:-4] + '-bin.jpg'
        # binary.save(outname)

        scale=to_gray_scale(diff,70)
        # outname = 'out/' + image_path[:-4] + '-scale.jpg'
        # scale.save(outname)

        """
        COMPTE
        """
        # print(np.array(scale))
        width, height = scale.size
        area = width * height
        num_true = np.sum(np.array(scale))
        prop_true = float(num_true/area) 
        # print(float(num_true/area))
        # prop_list.append(prop_true)

        labeled, nr_objects = ndimage.label(np.array(scale)) 
        # print("Number of objects is {}".format(nr_objects))
        # num_list.append(nr_objects)
        # result_list.append([date,prop_true,nr_objects])
        return_dict[proc_num] = [image_path,prop_true,nr_objects]

def compare_image_to_average_resize(chunk, average_image,proc_num, return_dict,ratio=0.1):

    im2 = average_image
    return_list = []

    for image_path in chunk :
        
        im1 = Image.open(image_path)
        w,h=im1.size
        newsize = (int(w*ratio),int(h*ratio))
        im_resize = im1.resize(newsize)
        date = get_date_taken(image_path)
        # date_list.append(date) 

        diff = ImageChops.difference(im2, im_resize)
        # outname = image_path[:-4] + '-diff.jpg'
        # diff.save(outname)

        # binary = diff.convert('1')
        # outname = image_path[:-4] + '-bin.jpg'
        # binary.save(outname)

        scale=to_gray_scale(diff,70)
        # outname = image_path[:-4] + '-scale.jpg'
        # scale.save(outname)

        """
        COMPTE
        """
        # print(np.array(scale))
        width, height = scale.size
        area = width * height
        num_true = np.sum(np.array(scale))
        prop_true = float(num_true/area) 
        # print(float(num_true/area))
        # prop_list.append(prop_true)

        labeled, nr_objects = ndimage.label(np.array(scale)) 
        # print("Number of objects is {}".format(nr_objects))
        # num_list.append(nr_objects)
        # result_list.append([date,prop_true,nr_objects])
        return_list.append([image_path,prop_true,nr_objects])
    return_dict[proc_num] = return_list


def chunkIt(seq, num):
    avg = len(seq) / float(num)
    out = []
    last = 0.0

    while last < len(seq):
        out.append(seq[int(last):int(last + avg)])
        last += avg

    return out


def multi_proc_manager(image_list, average_image, nlist):
    
    # print("Number of cpu : ", multiprocessing.cpu_count()) 
    cpu_num = int(multiprocessing.cpu_count() - 6)

    chunked_list = chunkIt(image_list, cpu_num)

    manager = multiprocessing.Manager()
    return_dict = manager.dict()

    procs = []

    proc_num=0

    for chunk in chunked_list:
        
        proc_num +=1
        proc_num_abs = str(nlist) + "_" + str(proc_num)

        # creating processes 
        proc = multiprocessing.Process(target=compare_image_to_average_resize, args=(chunk, average_image,proc_num_abs, return_dict)) 
        # proc = multiprocessing.Process(target=compare_image_to_average, args=(chunk, average_image,proc_num_abs, return_dict)) 
        procs.append(proc)
        proc.start()
  
    # starting processes
    for proc in procs:
        
        proc.join() 

    return return_dict

    # result_df = pd.concat(list(return_dict.values()))

    # print(result_df)
    # result_df.to_csv(outname, index=False)

    # all processes finished 
    # print("refine done!") 





# origine = os.path.dirname(os.path.abspath(__file__))


if __name__ == "__main__":


    threshold_array = color_array = np.array([ 
          [255, 255, 255],  # black not too green
          [200, 200, 200]])  # black

    mask = np.all(
    np.logical_and(
        np.min(threshold_array, axis=0) < im2,
        im2 < np.max(threshold_array, axis=0)
    ),
    axis=-1
    )
    im_mask = img_frombytes(mask)
    im_mask.show()



    blur_radius = 1.0
    threshold = 0
    n_image = 0
    # blur = False
    blur = True



    mask = img_threshold(image=image_path, threshold_array = threshold_array_bla)


    """
    ISOLATION OBJETS et BBX
    """

    measurements = bbx_from_mask(mask)

    # print(measurements)

    label_file_from_coord(measurements,image_path)



    """
    Show
    """

    # Create figure and axes
    fig, ax = plt.subplots(1)

    # Display the image
    im = Image.open(image_path)
    ax.imshow(im)

    for coordinates in measurements :

        left_x = int(coordinates[0] - int(0.5 * coordinates[2]))
        top_y = int(coordinates[1] - int(0.5 * coordinates[3]))
        w_obj = coordinates[2]
        h_obj = coordinates[3]

        area = w_obj * h_obj
        # print(area)

        if (area>7000 and area<50000):

            rect = patches.Rectangle((left_x, top_y), w_obj, h_obj, linewidth=1, edgecolor='r', facecolor='none')

            # Add the patch to the Axes
            ax.add_patch(rect)
        
    plot_name = 'plots/' + image_file[:-4] + '-plot.jpg'
    plt.savefig(plot_name)




    #     """
    #     SEUILLAGE
    #     """

    #     # # threshold_array_bla = color_array = np.array([ 
    #     # #       [55, 55, 55],  # black not too green
    #     # #       [0, 0, 0]])  # black


    #     # threshold_array_bla = color_array = np.array([ 
    #     #       [70, 70, 70],  # black not too green
    #     #       [0, 0, 0]])  # black

    #     # # threshold_array_bla = color_array = np.array([ 
    #     # #     [0, 255, 0],  # green
    #     # #     [150, 100, 150]])  # mid green

    #     """
    #     COMPTE
    #     """
    #     # print(np.array(scale))
    #     width, height = scale.size
    #     area = width * height
    #     num_true = np.sum(np.array(scale))
    #     prop_true = float(num_true/area) 
    #     print(float(num_true/area))
    #     prop_list.append(prop_true)

    #     labeled, nr_objects = ndimage.label(np.array(scale)) 
    #     print("Number of objects is {}".format(nr_objects))
    #     num_list.append(nr_objects)


    # fig, ax1 = plt.subplots()

    # color = 'tab:red'
    # ax1.set_xlabel('date')
    # ax1.set_ylabel('Proportion of diff. px', color=color)
    # ax1.plot(date_list, prop_list, color=color)
    # ax1.tick_params(axis='y', labelcolor=color)

    # ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

    # color = 'tab:blue'
    # ax2.set_ylabel('Number of objects', color=color)  # we already handled the x-label with ax1
    # ax2.plot(date_list, num_list, color=color)
    # ax2.tick_params(axis='y', labelcolor=color)

    # fig.tight_layout()  # otherwise the right y-label is slightly clipped
    # plt.show()

    



















    # t0 = time.time()
    
    # dir_to_detect = 'test'
    # average_num = 10 # how many images are used to generate average pic.
    
    # # get all files in dir 
    # all_images = sorted([os.path.join(root, name)
    #          for root, dirs, files in os.walk(dir_to_detect)
    #          for name in files
    #          if name.endswith((".JPG"))])
    
    # # print(all_images)

    # chunks = [all_images[x:x+average_num] for x in range(0, len(all_images), average_num)]
    # n = len(chunks)
    # # print(chunks[0])
    # # result_list = []
    # nlist = 0 # to ensure that each process identifier is unique
    # final_dic = {}

    # for image_list in chunks :
    #     nlist += 1
    #     print(str(nlist)+ "/" + str(n))
    #     print("getting average image")
    #     average_image = get_average_image_list_resize(image_list)

    #     print(image_list)

    #     print("comparing..")
    #     return_dict = multi_proc_manager(image_list,average_image,nlist)
    #     # print(len(return_dict))
    #     final_dic.update(return_dict)

    #     if nlist%2 == 0:
    #         print("pause....")
    #         time.sleep(2)
    #     if nlist%10 == 0:
    #         print("pause....")
    #         time.sleep(10)
        
    
    # result_list_temp = list(final_dic.values())
    
    # result_list = []

    # for interm_list in result_list_temp:
    #     for result in interm_list:
    #         result_list.append(result)
    
    # print(len(result_list))

    # date_list = []
    # prop_list = []
    # num_list = []
    # result_list = sorted(result_list)

    # for result in result_list :
    #     date_list.append(result[0])
    #     prop_list.append(result[1])
    #     num_list.append(result[2])

    # df = pd.DataFrame(list(zip(date_list, prop_list, num_list)), 
    #            columns =['image', 'prop_diff_to_avg','num_diff_to_avg'])

    # df.to_csv('results.csv',index=False) 

    # print(num_list)
    # t1 = time.time()

    # print((t1-t0)/60)

    # fig, ax1 = plt.subplots()

    # color = 'tab:red'
    # ax1.set_xlabel('date')
    # ax1.set_ylabel('Proportion of diff. px', color=color)
    # ax1.plot(date_list, prop_list, color=color)
    # ax1.tick_params(axis='y', labelcolor=color)
    # ax1.tick_params(axis='x', rotation=90, labelsize=5)
    # ax1.set_xticks(ax1.get_xticks()[::2])

    # ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

    # color = 'tab:blue'
    # ax2.set_ylabel('Number of objects', color=color)  # we already handled the x-label with ax1
    # ax2.plot(date_list, num_list, color=color)
    # ax2.tick_params(axis='y', labelcolor=color)

    # fig.tight_layout()  # otherwise the right y-label is slightly clipped
    # plt.show()
    # plt.savefig("ex_fig.png",dpi=300)





# if __name__ == "__main__":

#     t0 = time.time()
    
#     dir_to_detect = 'test'
#     average_num = 30 # how many images are used to generate average pic.
    
#     # get all files in dir 
#     all_images = sorted([os.path.join(root, name)
#              for root, dirs, files in os.walk(dir_to_detect)
#              for name in files
#              if name.endswith((".JPG"))])
    
#     # print(all_images)

#     chunks = [all_images[x:x+average_num] for x in range(0, len(all_images), average_num)]
#     n = len(chunks)
#     # print(chunks[0])
#     # result_list = []
#     nlist = 0 # to ensure that each process identifier is unique
#     final_dic = {}

#     for image_list in chunks :
#         nlist += 1
#         print(str(nlist)+ "/" + str(n))
#         print("getting average image")
#         average_image = get_average_image_list(image_list)

#         print("comparing..")
#         return_dict = multi_proc_manager(image_list,average_image,nlist)
#         # print(return_dict)
#         final_dic.update(return_dict)

#         print("pause....")
#         time.sleep(10)
    
    
#     result_list = list(final_dic.values())
#     # print(result_list)

#     date_list = []
#     prop_list = []
#     num_list = []
#     result_list = sorted(result_list)

#     for result in result_list :
#         date_list.append(result[0])
#         prop_list.append(result[1])
#         num_list.append(result[2])

#     print(num_list)
#     t1 = time.time()

#     print((t1-t0 - nlist*10)/60)

#     fig, ax1 = plt.subplots()

#     color = 'tab:red'
#     ax1.set_xlabel('date')
#     ax1.set_ylabel('Proportion of diff. px', color=color)
#     ax1.plot(date_list, prop_list, color=color)
#     ax1.tick_params(axis='y', labelcolor=color)
#     ax1.tick_params(axis='x', rotation=90)
#     ax1.set_xticks(ax1.get_xticks()[::2])

#     ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

#     color = 'tab:blue'
#     ax2.set_ylabel('Number of objects', color=color)  # we already handled the x-label with ax1
#     ax2.plot(date_list, num_list, color=color)
#     ax2.tick_params(axis='y', labelcolor=color)

#     fig.tight_layout()  # otherwise the right y-label is slightly clipped
#     plt.show()





















    

    # # get_average_image(dir_to_detect)

    # files_to_list = sorted([f for f in os.listdir(dir_to_detect) if f.endswith('.JPG')])

    # print(files_to_list)
    # n = len(files_to_list)
    # i = 0
    # date_list = []
    # prop_list = []
    # num_list = []

    # for image_file in files_to_list:
    #     i+=1
        
    #     print(image_file + " (" + str(i) + "/" + str(n) +")")

    #     image_path = dir_to_detect + "/" + image_file




    #     im2 = Image.open('Average.png')
    #     im1 = Image.open(image_path)
    #     date = get_date_taken(image_path)
    #     date_list.append(date) 

    #     diff = ImageChops.difference(im2, im1)
    #     # outname = image_path[:-4] + '-diff.jpg'
    #     # diff.save(outname)

    #     # binary = diff.convert('1')
    #     # outname = image_path[:-4] + '-bin.jpg'
    #     # binary.save(outname)

    #     scale=to_gray_scale(diff,70)
    #     outname = image_path[:-4] + '-scale.jpg'
    #     # scale.save(outname)


    #     """
    #     SEUILLAGE
    #     """

    #     # # threshold_array_bla = color_array = np.array([ 
    #     # #       [55, 55, 55],  # black not too green
    #     # #       [0, 0, 0]])  # black


    #     # threshold_array_bla = color_array = np.array([ 
    #     #       [70, 70, 70],  # black not too green
    #     #       [0, 0, 0]])  # black

    #     # # threshold_array_bla = color_array = np.array([ 
    #     # #     [0, 255, 0],  # green
    #     # #     [150, 100, 150]])  # mid green

    #     """
    #     COMPTE
    #     """
    #     # print(np.array(scale))
    #     width, height = scale.size
    #     area = width * height
    #     num_true = np.sum(np.array(scale))
    #     prop_true = float(num_true/area) 
    #     print(float(num_true/area))
    #     prop_list.append(prop_true)

    #     labeled, nr_objects = ndimage.label(np.array(scale)) 
    #     print("Number of objects is {}".format(nr_objects))
    #     num_list.append(nr_objects)


    # fig, ax1 = plt.subplots()

    # color = 'tab:red'
    # ax1.set_xlabel('date')
    # ax1.set_ylabel('Proportion of diff. px', color=color)
    # ax1.plot(date_list, prop_list, color=color)
    # ax1.tick_params(axis='y', labelcolor=color)

    # ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

    # color = 'tab:blue'
    # ax2.set_ylabel('Number of objects', color=color)  # we already handled the x-label with ax1
    # ax2.plot(date_list, num_list, color=color)
    # ax2.tick_params(axis='y', labelcolor=color)

    # fig.tight_layout()  # otherwise the right y-label is slightly clipped
    # plt.show()

    


    #     # threshold_array = color_array = np.array([ 
    #     #       [255, 255, 255],  # black not too green
    #     #       [200, 200, 200]])  # black

    #     # mask = np.all(
    #     # np.logical_and(
    #     #     np.min(threshold_array, axis=0) < im2,
    #     #     im2 < np.max(threshold_array, axis=0)
    #     # ),
    #     # axis=-1
    #     # )
    #     # im_mask = img_frombytes(mask)
    #     # im_mask.show()



    #     # blur_radius = 1.0
    #     # threshold = 0
    #     # n_image = 0
    #     # # blur = False
    #     # blur = True



    #     # mask = img_threshold(image=image_path, threshold_array = threshold_array_bla)


    #     # """
    #     # ISOLATION OBJETS et BBX
    #     # """

    #     # measurements = bbx_from_mask(mask)

    #     # # print(measurements)

    #     # label_file_from_coord(measurements,image_path)



    #     # """
    #     # Show
    #     # """

    #     # # # Create figure and axes
    #     # # fig, ax = plt.subplots(1)

    #     # # # Display the image
    #     # # im = Image.open(image_path)
    #     # # ax.imshow(im)

    #     # # for coordinates in measurements :

    #     # #     left_x = int(coordinates[0] - int(0.5 * coordinates[2]))
    #     # #     top_y = int(coordinates[1] - int(0.5 * coordinates[3]))
    #     # #     w_obj = coordinates[2]
    #     # #     h_obj = coordinates[3]

    #     # #     area = w_obj * h_obj
    #     # #     # print(area)

    #     # #     if (area>7000 and area<50000):

    #     # #         rect = patches.Rectangle((left_x, top_y), w_obj, h_obj, linewidth=1, edgecolor='r', facecolor='none')

    #     # #         # Add the patch to the Axes
    #     # #         ax.add_patch(rect)
            
    #     # # plot_name = 'plots/' + image_file[:-4] + '-plot.jpg'
    #     # # plt.savefig(plot_name)
