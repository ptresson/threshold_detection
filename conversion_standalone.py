from __future__ import division
import glob
import os
import math
import time
import tqdm
# import torch
# import torch.nn as nn
# import torch.nn.functional as F
# from torch.autograd import Variable
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import shutil

def remove_some_files(dir,extension = '_convert.txt'):
    
    files_to_rn = [f for f in os.listdir(dir) if f.endswith('.txt')]

    for file in files_to_rn :
    
        if not file.endswith(extension) :
            print(file)
            full_name = dir + '/' + file
            os.remove(full_name)



def rename_some_files(dir_to_rn,old_ext='_new.txt', new_ext='.txt'):

    files_to_rn = [f for f in os.listdir(dir_to_rn) if f.endswith(old_ext)]
    print(files_to_rn)
    len_ext = len(old_ext)

    # changement d'extension
    for file in files_to_rn:
        name_with_path = dir_to_rn + '/' + file
        base = name_with_path[:-len_ext]
        new_name_with_path = base + new_ext
        print(new_name_with_path)
        os.rename(name_with_path, new_name_with_path)
    



def convert_al_labels(dir_to_convert='labels',new_class='9'):
       
    # list txt files
    txt_labels = sorted([f for f in os.listdir(dir_to_convert) if f.endswith('.txt')])

    # change first char of every line of every file according to labels
    for file in txt_labels :
        print(file)
        full_name = os.path.join(dir_to_convert, file)
        full_name_no_ext = full_name[:-4]
        outname = full_name_no_ext + '_convert.txt' # new_labels are named *_convert.txt
        new_label = open(outname,"w")
        
        with open(full_name) as f:
            for line in f:
                line = list(line)
                # line = line[2:] # to remove several first char
                line[0] = new_class
                line_str = "".join(line)
                print(line_str)
                new_label.write(line_str)
                # print(line)
        new_label.close()

    # remove all .txt files except _convert.txt
    remove_some_files(dir_to_convert)
    rename_some_files(dir_to_convert,old_ext='_convert.txt',new_ext='.txt')





convert_al_labels()