import os, numpy, PIL
from PIL import Image, ImageDraw, ImageFilter


def add_layers(background_file, foreground_file, alpha=0.5):
    
    background = Image.open(background_file)
    foreground = Image.open(foreground_file)
    foreground.convert('RGBA')

    alpha_abs = int(255 * alpha)
    foreground.putalpha(alpha_abs)

    background.paste(foreground, (0, 0), foreground)
        
    return background



def change_contrast(img, level):
    factor = (259 * (level + 255)) / (255 * (259 - level))
    def contrast(c):
        return 128 + factor * (c - 128)
    return img.point(contrast)


def get_average_image(folder='test'):

    # Access all PNG files in directory
    allfiles=os.listdir(folder)
    imlist_without_path=[filename for filename in allfiles if  filename[-4:] in [".png",".JPG"]]
    prefix = folder + '/'
    imlist = [prefix + image for image in imlist_without_path]

    # Assuming all images are the same size, get dimensions of first image
    w,h=Image.open(imlist[0]).size
    N=len(imlist)

    # Create a numpy array of floats to store the average (assume RGB images)
    arr=numpy.zeros((h,w,3),numpy.float)

    # Build up average pixel intensities, casting each image as an array of floats
    for im in imlist:
        imarr=numpy.array(Image.open(im),dtype=numpy.float)
        arr=arr+imarr/N

    # Round values in array and cast as 8-bit integer
    arr=numpy.array(numpy.round(arr),dtype=numpy.uint8)

    # Generate, save and preview final image
    out=Image.fromarray(arr,mode="RGB")
    out.save("Average.png")
    # out.show()

def get_average_image_list(imlist):

    # Assuming all images are the same size, get dimensions of first image
    w,h=Image.open(imlist[0]).size
    N=len(imlist)

    # Create a numpy array of floats to store the average (assume RGB images)
    arr=numpy.zeros((h,w,3),numpy.float)

    # Build up average pixel intensities, casting each image as an array of floats
    for im in imlist:
        imarr=numpy.array(Image.open(im),dtype=numpy.float)
        arr=arr+imarr/N

    # Round values in array and cast as 8-bit integer
    arr=numpy.array(numpy.round(arr),dtype=numpy.uint8)

    # Generate, save and preview final image
    out=Image.fromarray(arr,mode="RGB")
    out.save("Average.png")
    # out.show()
    return out

def get_average_image_list_resize(imlist, ratio=0.1):

    # Assuming all images are the same size, get dimensions of first image
    w,h=Image.open(imlist[0]).size
    w_resize = int(w*ratio)
    h_resize = int(h*ratio)
    N=len(imlist)

    # Create a numpy array of floats to store the average (assume RGB images)
    arr=numpy.zeros((h_resize,w_resize,3),numpy.float)

    # Build up average pixel intensities, casting each image as an array of floats
    for im in imlist:
        im_orig = Image.open(im)
        im_resize = im_orig.resize((w_resize,h_resize))
        imarr=numpy.array(im_resize,dtype=numpy.float)
        arr=arr+imarr/N

    # Round values in array and cast as 8-bit integer
    arr=numpy.array(numpy.round(arr),dtype=numpy.uint8)

    # Generate, save and preview final image
    out=Image.fromarray(arr,mode="RGB")
    # out.save("Average.png")
    # out.show()
    return out

# get_average_image('test')
# merged = add_layers('fond.JPG','total.JPG',0.5)
# merged.save('merged_fond.png')
# final = change_contrast(merged, 100)
# final.show()
# final.save('final_fond.png')
