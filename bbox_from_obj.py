import os
from os import listdir
from os.path import isfile, join
from PIL import Image
import numpy as np
from scipy import ndimage
import matplotlib.pyplot as plt


def bbx_from_mask(mask, blur_radius = 0, buffer=False, area_threshold_min = 100, area_threshold_max = 300 ):

    a = ndimage.gaussian_filter(mask, blur_radius)
    im_a = Image.fromarray(a)
    # im_a.save('gaussian.jpg')
    
    # Label objects
    labeled_image, num_features = ndimage.label(a)
    # Find the location of all objects
    objs = ndimage.find_objects(labeled_image)
    
    # Get the height and width and center
    measurements = []
    for ob in objs:
        
        h_abs = int(ob[0].stop - ob[0].start)
        y_abs = int(ob[0].start + (h_abs / 2) )
        w_abs = int(ob[1].stop - ob[1].start)
        x_abs = int(ob[1].start + (w_abs / 2) )
        
        if buffer:
            w_abs = int(w_abs * 2.5)
            h_abs = int(h_abs * 2.5) 

        

        # threshold to filter small particles
        if w_abs * h_abs > area_threshold_min and w_abs * h_abs < area_threshold_max :

            coordinates = [x_abs, y_abs, w_abs, h_abs]
            measurements.append(coordinates)

    return measurements

def label_file_from_coord(measurment_list, image_path, out_dir = 'labels', thr_min = 0, thr_max = 50000):

    image_ext = image_path.split('/')[-1]
    image_name = image_ext.split('.')[0]
    print(image_name)

    im = Image.open(image_path)
    width, height = im.size
    
    label_name = out_dir + '/' + image_name + '.txt'
    print(label_name)

    new_label = open(label_name,"w")

    for coordinates in measurment_list:
        
        x_abs = coordinates[0]
        y_abs = coordinates[1]
        w_abs = coordinates[2]
        h_abs = coordinates[3]
        area = w_abs * h_abs
        # print(area)

        if (area>thr_min and area<thr_max):

            x = float(x_abs/width)
            y = float(y_abs/height)
            w = float(w_abs/width)
            h = float(h_abs/height)
            # print(x,y,w,h)
            
            new_label.write("CLASS %4f %4f %4f %4f\n" % (x,y,w,h))

    new_label.close()

