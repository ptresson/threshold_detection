import os
import pandas as pd
import multiprocessing
import time
from os import listdir
from os.path import isfile, join
from PIL import Image, ImageChops
import numpy as np
from scipy import ndimage
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.ticker as ticker


from seuillage import img_threshold, change_contrast, change_contrast_folder, to_gray_scale, img_frombytes
from bbox_from_obj import bbx_from_mask, label_file_from_coord
from add_layers import add_layers, get_average_image, get_average_image_list,get_average_image_list_resize

def chunkIt(seq, num):
    avg = len(seq) / float(num)
    out = []
    last = 0.0

    while last < len(seq):
        out.append(seq[int(last):int(last + avg)])
        last += avg

    return out

def get_date_taken(path):
    return Image.open(path)._getexif()[36867]

def multi_proc_manager(image_list,target_function,ratio=0.1,area_threshold_min=10,area_threshold_max=30,pixel_value=10):
    
    # print("Number of cpu : ", multiprocessing.cpu_count()) 
    cpu_num = int(multiprocessing.cpu_count() - 6)

    chunked_list = chunkIt(image_list, cpu_num)

    manager = multiprocessing.Manager()
    return_dict = manager.dict()

    procs = []

    proc_num=0

    for chunk in chunked_list:
        
        proc_num +=1

        # creating processes 
        proc = multiprocessing.Process(target=target_function, args=(chunk,return_dict,ratio,area_threshold_min,area_threshold_max,pixel_value)) 
        # proc = multiprocessing.Process(target=compare_image_to_average, args=(chunk, average_image,proc_num_abs, return_dict)) 
        procs.append(proc)
        proc.start()
  
    # starting processes
    for proc in procs:
        
        proc.join() 

    return return_dict

    # result_df = pd.concat(list(return_dict.values()))

    # print(result_df)
    # result_df.to_csv(outname, index=False)

    # all processes finished 
    # print("refine done!") 


def image_processing(image_path, return_dict, ratio = 0.1):

    image = Image.open(image_path)

    ############# Resize ################

    w,h=image.size
    newsize = (int(w*ratio),int(h*ratio))
    image = image.resize(newsize)

    ############ Threhold ###############

    threshold_array = np.array([ 
          [30, 30, 30],  # dark grey
          [0, 0, 0]])  # black

    mask = np.all(
    np.logical_and(
        np.min(threshold_array, axis=0) < image,
        image < np.max(threshold_array, axis=0)
    ),
    axis=-1
    )
    im_mask = img_frombytes(mask)
    # outname = image_path[:-4] + '-mask.jpg'
    # im_mask.save(outname)

    ########## Detect ################

    measurements = bbx_from_mask(mask,area_threshold_min = 20,area_threshold_max=150)
    
    
    ########## returning length to tict for multiproc #########
    # print(len(measurements))
    # return len(measurements)
    return_dict[image_path] = len(measurements)
    print(return_dict)

    
def image_processing_multi(image_list, return_dict,ratio=0.1,area_threshold_min=10,area_threshold_max=30,pixel_value=10):

    i = 0
    for image_path in image_list:
        
        i += 1
        # print(str(i)+ "/" + str(len(image_list)))
        # image_processing(image_path,return_dict)
        image = Image.open(image_path)

        ############# Resize ################

        # w,h=image.size
        # newsize = (int(w*ratio),int(h*ratio))
        # image = image.resize(newsize)

        ############ Threhold ###############

        threshold_array = np.array([ 
            [pixel_value, pixel_value, pixel_value],  # dark grey
            [0, 0, 0]])  # black

        mask = np.all(
        np.logical_and(
            np.min(threshold_array, axis=0) < image,
            image < np.max(threshold_array, axis=0)
        ),
        axis=-1
        )
        im_mask = img_frombytes(mask)
        outname = image_path[:-4] + '-mask.JPG'
        im_mask.save(outname)

        ########## Detect ################

        # measurements = bbx_from_mask(mask,area_threshold_min = area_threshold_min,
                                            # area_threshold_max = area_threshold_max)
        
        
        ########## returning length to dict for multiproc #########
        # print(len(measurements))
        # return len(measurements)

        ###### TOTAL PIXELS ####
        n_black_pixels = np.sum(mask)

        return_dict[image_path] = n_black_pixels

        # return_dict[image_path] = len(measurements)
    #print(return_dict)







if __name__ == "__main__":

    t0 = time.time()
    
    
    dir_to_detect = 'test_short/'

    image_list = sorted([dir_to_detect + f for f in os.listdir(dir_to_detect) if f.endswith('.jpg')])
    # print(image_list)


    return_dict = multi_proc_manager(image_list,image_processing_multi,0.1,8,20,30)

    
    print(return_dict)
        # to plot dict
    lists = sorted(return_dict.items()) # sorted by key, return a list of tuples
    x, y = zip(*lists) # unpack a list of pairs into two tuples

    # print(x)
    # print(y)
    print(np.median(y))
    y2 = [elt - np.mean(y) for elt in y]

    t1 = time.time()
    print((t1-t0)/60)

    fig, ax1 = plt.subplots()

    color = 'tab:red'
    ax1.set_xlabel('date')
    ax1.set_ylabel('number of objects', color=color)
    ax1.plot(x, y2, color=color)
    ax1.tick_params(axis='y', labelcolor=color)
    ax1.tick_params(axis='x', rotation=90, labelsize=5)

    plt.show()
    # plt.savefig('object.png',dpi=150)






# origine = os.path.dirname(os.path.abspath(__file__))


# if __name__ == "__main__":

#     t0 = time.time()
    
    
#     dir_to_detect = 'test_short/'

#     image_list = sorted([dir_to_detect + f for f in os.listdir(dir_to_detect) if f.endswith('.JPG')])
#     # print(image_list)


#     ########################################### Pixel value calibration #####################

#     value_list = [15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,40]
        
#     results_list = []
#     i=0
#     n = len(value_list)

#     for thr_min in value_list:
    

#         i+=1
#         print(str(i)+'/'+str(n))
                

#         return_dict = multi_proc_manager(image_list,image_processing_multi,0.1,8,20,thr_min)

#         # to plot dict
#         lists = sorted(return_dict.items()) # sorted by key, return a list of tuples
#         x, y = zip(*lists) # unpack a list of pairs into two tuples


#         print(max(set(y), key=y.count))
        
        
#         print(sum(y)-2*len(y))

        
#         resid = sum([abs(measure-2) for measure in y])

#         results_list.append([thr_min,resid])
    
#         time.sleep(10)
    
#     print(results_list)

#     res = [elt[1] for elt in results_list ]

#     tmin = [elt[0] for elt in results_list ]

#     df = pd.DataFrame(list(zip(res, tmin)), 
#                columns =['res', 'value'])

#     df.to_csv('calibration_results_value.csv',index=False)

#     t1 = time.time()
#     print((t1-t0)/60)





    ######################################### area calibration ###########################
    # thr_min_list = [3,4,5,6,7,8,9,10,15,20,25]
    # thr_max_list = [4,5,6,7,8,9,10,15,20,25,30,40,50,60,80]
    
    # results_list = []
    # i=0
    # n = len(thr_max_list) * len(thr_min_list)

    # for thr_min in thr_min_list:
    #     for thr_max in thr_max_list:

    #         i+=1
    #         print(str(i)+'/'+str(n))
                    

    #         return_dict = multi_proc_manager(image_list,image_processing_multi,0.1,thr_min,thr_max)

    #         # to plot dict
    #         lists = sorted(return_dict.items()) # sorted by key, return a list of tuples
    #         x, y = zip(*lists) # unpack a list of pairs into two tuples


    #         print(max(set(y), key=y.count))
            
            
    #         print(sum(y)-2*len(y))

            
    #         resid = sum([abs(measure-2) for measure in y])

    #         results_list.append([thr_min,thr_max,resid])
        
    #     time.sleep(10)
    
    # print(results_list)

    # res = [elt[2] for elt in results_list ]

    # tmax = [elt[1] for elt in results_list ]

    # tmin = [elt[0] for elt in results_list ]

    # df = pd.DataFrame(list(zip(res, tmin, tmax)), 
    #            columns =['res', 'tmin','tmax'])

    # df.to_csv('calibration_results.csv',index=False)

    # t1 = time.time()
    # print((t1-t0)/60)

    # fig, ax1 = plt.subplots()

    # color = 'tab:red'
    # ax1.set_xlabel('date')
    # ax1.set_ylabel('number of objects', color=color)
    # ax1.plot(x, y, color=color)
    # ax1.tick_params(axis='y', labelcolor=color)
    # ax1.tick_params(axis='x', rotation=90, labelsize=5)

    # # plt.show()
    # plt.savefig('object.png',dpi=150)
   




