import os
from os import listdir
from os.path import isfile, join
from PIL import Image
import numpy as np
from scipy import ndimage
import matplotlib.pyplot as plt

# c_yel = color_array = np.array([ 
#   [255, 255, 0],  # bright yellow
#   [150, 150, 150]])  # mid yellow

blur_radius = 1.0
threshold = 0
n_image = 0
# blur = False
blur = True


def img_frombytes(data):
    size = data.shape[::-1]
    databytes = np.packbits(data, axis=1)
    return Image.frombytes(mode='1', size=size, data=databytes)

def img_threshold(image,threshold_array):

    im = Image.open(image)
    width, height = im.size
    # width2 = int(width/4)
    # height2 = int(height/4)
    width2 = width
    height2 = height
    im2 = im.resize((width2,height2), Image.ANTIALIAS)
    im2 = np.asarray(im2)
    print(im2.shape)

    mask = np.all(
    np.logical_and(
        np.min(threshold_array, axis=0) < im2,
        im2 < np.max(threshold_array, axis=0)
    ),
    axis=-1
    )
    im_mask = img_frombytes(mask)
    im_mask.save('masks/' + str(image[:-4])+'_thr_resized.jpg')
    
    if blur:
        imgf = ndimage.gaussian_filter(im_mask, blur_radius)
        labeled, nr_objects = ndimage.label(imgf > threshold) 
        print("Number of objects is {}".format(nr_objects))
    else:
        imgf = ndimage.gaussian_filter(im_mask, blur_radius)
        labeled, nr_objects = ndimage.label(mask > threshold) 
        print("Number of objects is {}".format(nr_objects))
    
    return mask

def change_contrast(img, level):
    factor = (259 * (level + 255)) / (255 * (259 - level))
    def contrast(c):
        return 128 + factor * (c - 128)
    return img.point(contrast)

def change_contrast_folder(orig_folder='151_PANA', out_folder='contrast', level=-50):

    files_to_list = sorted([f for f in os.listdir(orig_folder) if (f.endswith('.JPG')and not f.endswith('-contrast.JPG'))])
    print(files_to_list)

    for image_file in files_to_list:
        
        image_path = orig_folder + "/" + image_file

        print(image_path)

        im_orig = Image.open(image_path)

        im_contrast = change_contrast(im_orig,level)
        # outname = image_file[:-4] + "-contrast.JPG"
        outname = out_folder + "/" + image_file

        im_contrast.save(outname)

def to_gray_scale(img, thresh=200):
    fn = lambda x : 255 if x > thresh else 0
    r = img.convert('L').point(fn, mode='1')
    return r


# def otsu(error):
#     sig_max, opti_tresh = tf.reduce_sum(tf.zeros_like(error), axis=[1, 2]), \
#                           tf.reduce_sum(tf.zeros_like(error), axis=[1, 2])

#     for eps in np.arange(0.1, 0.91, 0.01):

#         cond0 = tf.where(error < eps, error, tf.zeros_like(error))
#         cond1 = tf.where(error >= eps, error, tf.zeros_like(error))

#         N0 = tf.math.count_nonzero(cond0, axis=[1, 2], dtype=tf.float32)
#         N1 = tf.math.count_nonzero(cond1, axis=[1, 2], dtype=tf.float32)

#         mean0 = tf.reduce_mean(cond0, axis=[1, 2])*((N1+N0)/N0)
#         mean0 = tf.where(tf.math.is_nan(mean0), tf.zeros_like(mean0), mean0)
#         mean1 = tf.reduce_mean(cond1, axis=[1, 2])*((N1+N0)/N1)
#         mean1 = tf.where(tf.math.is_nan(mean1), tf.zeros_like(mean1), mean1)


#         prob0 = N0/(N1+N0)
#         prob1 = N1/(N1+N0)

#         sig_b = prob0*prob1*(mean0-mean1)**2
#         bool = tf.math.greater_equal(sig_b, sig_max)
#         sig_max = tf.where(bool, sig_b, sig_max)
#         opti_tresh = tf.where(bool, tf.zeros_like(opti_tresh)+eps, opti_tresh)

#     return opti_tresh 

# def otsu(gray):
#     pixel_number = gray.shape[0] * gray.shape[1]
#     mean_weigth = 1.0/pixel_number
#     his, bins = np.histogram(gray, np.array(range(0, 256)))
#     final_thresh = -1
#     final_value = -1
#     for t in bins[1:-1]: # This goes from 1 to 254 uint8 range (Pretty sure wont be those values)
#         Wb = np.sum(his[:t]) * mean_weigth
#         Wf = np.sum(his[t:]) * mean_weigth

#         mub = np.mean(his[:t])
#         muf = np.mean(his[t:])

#         value = Wb * Wf * (mub - muf) ** 2

#         print("Wb", Wb, "Wf", Wf)
#         print("t", t, "value", value)

#         if value > final_value:
#             final_thresh = t
#             final_value = value
#     final_img = gray.copy()
#     print(final_thresh)
#     final_img[gray > final_thresh] = 255
#     final_img[gray < final_thresh] = 0
#     return final_img